package essential_ib.enums;

public enum Period {
	THIS_MONTH,
	LAST_MONTH,
	THIS_QUARTER,
	LAST_QUARTER,
	HALF_YEAR
}
