package essential_ib.enums;

import java.util.NoSuchElementException;

public enum AccountType {

	CHECKING("Girokonto"),
	SAVING("Sparkonto");

	private final String value;

	AccountType(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}

	public static AccountType findByString(String str) throws NoSuchElementException {
		for (AccountType type : values()) {
			// find by name
			switch (str) {
			case "checking":
				return CHECKING;

			case "saving":
				return SAVING;
			}

			// find by value
			if (type.value.equals(str)) {
				return type;
			}
		}

		throw new NoSuchElementException(str + " is not a valid AccountType");
	}
}
