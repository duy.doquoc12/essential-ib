package essential_ib.services;

import essential_ib.entities.Person;

public interface PersonService {

	/**
	 * Creates a new person or update its properties in the database.
	 *
	 * @param person person
	 */
	void persist(Person person);

	/**
	 * Find person by first name and last name.
	 *
	 * @param firstName first name
	 * @param lastName  last name
	 * @return person or null if not exists
	 */
	Person findByFirstNameAndLastName(String firstName, String lastName);
}
