package essential_ib.services;

import essential_ib.entities.User;

public interface UserService {

	/**
	 * Find user by its user name.
	 *
	 * @param userName user name
	 * @return user with user name; or null if not exists
	 */
	User findByUserName(String userName);

	/**
	 * Find user by its user name and password.
	 *
	 * @param userName user name
	 * @param password password
	 * @return user with user name and password; or null if not exists
	 */
	User findByUserNameAndPassword(String userName, String password);

	/**
	 * Find user by its id.
	 *
	 * @param id ID
	 * @return user with id; or null if not exists
	 */
	User findById(Long id);
}
