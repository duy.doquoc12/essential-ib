package essential_ib.services;

import essential_ib.entities.Transfer;
import essential_ib.enums.Period;

import java.util.List;

public interface TransferService {

	/**
	 * Create new transfer or update already existing ones with given transfer.
	 *
	 * @param transfer transfer
	 * @return saved or updated transfer
	 */
	Transfer persist(Transfer transfer);

	/**
	 * Transfer given amount of money from sender account to receiver account.
	 *
	 * @param transfer transfer object containing all information of the transfer
	 */
	void execute(Transfer transfer);

	/**
	 * Find all transfer of the given account within the given period.
	 *
	 * @param accId  id of account
	 * @param period period
	 * @return list of transfers
	 */
	List<Transfer> findTransferListOfAccountForPeriod(Long accId, Period period);

	/**
	 * Get all transfers of an user, where the saved attribute is true.<br>
	 * Order by create date descending.
	 */
	List<Transfer> getSavedTransfersOfUser(Long userId);

	/**
	 * Find transfer with given id.
	 *
	 * @param id transfer id
	 * @return transfer with given id; or null if not exist
	 */
	Transfer findById(Long id);

	/**
	 * Delete transfer.
	 *
	 * @param transfer transfer
	 */
	void deleteTransfer(Transfer transfer);
}
