package essential_ib.services;

import essential_ib.entities.Account;
import essential_ib.enums.AccountType;

import java.util.List;

public interface AccountService {

	/**
	 * If account doesn't exist, then create a new account.<br>
	 * Else update old account.
	 *
	 * @param account account
	 */
	void persist(Account account);

	/**
	 * Find account by its account id.
	 *
	 * @param iban account id
	 * @return account; or null if not exists
	 */
	Account findByIban(String iban);

	/**
	 * Find all accounts of an user
	 *
	 * @param userId user id
	 * @return list of accounts
	 */
	List<Account> findByUserId(Long userId);

	/**
	 * Find account of user with account type and name.
	 *
	 * @param userId user id
	 * @param type   account type
	 * @param name   account name
	 * @return account; or null if not exists
	 */
	Account findByUserIdAndTypeAndName(Long userId, AccountType type, String name);
	
}
