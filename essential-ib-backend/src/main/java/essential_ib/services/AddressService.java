package essential_ib.services;

import essential_ib.entities.Address;

import java.util.List;

public interface AddressService {

	/**
	 * If address doesn't exist, then create a new address.<br>
	 * Else update old address.
	 *
	 * @param address address
	 */
	Address persist(Address address);

	/**
	 * Find address by all its attributes.
	 *
	 * @param street      street
	 * @param houseNumber houseNumber
	 * @param zipCode     zipCode
	 * @param place       place
	 * @param country     country
	 * @param stair       stair
	 * @param doorNumber  doorNumber
	 * @return address list
	 */
	List<Address> find(String street, String houseNumber, String zipCode, String place, String country, String stair,
			String doorNumber);
}
