package essential_ib.repositories;

import essential_ib.entities.Address;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface AddressRepository extends CrudRepository<Address, Long> {

	/**
	 * Select * from address <br>
	 * where street=:street and<br>
	 * &emsp; houseNumber=:houseNumber and <br>
	 * &emsp; zipCode=:zipCode and<br>
	 * &emsp; place=:place and <br>
	 * &emsp; country=:country and <br>
	 * &emsp; stair=:stair and <br>
	 * &emsp; doorNumber=:doorNumber;
	 *
	 * @param street      street
	 * @param houseNumber house number
	 * @param zipCode     zip code
	 * @param place       place
	 * @param country     country
	 * @param stair       stair
	 * @param doorNumber  door number
	 * @return address lists; could be empty but is never null
	 */
	List<Address> findByStreetAndHouseNumberAndZipCodeAndPlaceAndCountryAndStairAndDoorNumber(String street,
			String houseNumber, String zipCode, String place, String country, String stair, String doorNumber);
}
