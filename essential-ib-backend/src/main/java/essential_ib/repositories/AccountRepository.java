package essential_ib.repositories;

import essential_ib.entities.Account;
import essential_ib.entities.User;
import essential_ib.enums.AccountType;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface AccountRepository extends CrudRepository<Account, Long> {

	/**
	 * Select * from account where iban=:iban;
	 *
	 * @param iban account id
	 * @return account with iban or null, if not exists
	 */
	Account findByIban(String iban);

	/**
	 * Select a.* from account a
	 * join user_account ua on ua.ACCOUNT_ID=a.ID
	 * where ua.USER_ID=:user.id;
	 *
	 * @param user accounts owner
	 * @return list of accounts
	 */
	List<Account> findByUsers(User user);

	/**
	 * Select a.* from account a
	 * join user_account ua on ua.ACCOUNT_ID=a.ID
	 * where ua.USER_ID=:user.id and a.TYPE=:type and a.NAME=:accName;
	 *
	 * @param user account owner
	 * @param type account type
	 * @param accName account name
	 * @return account; or null if not exists
	 */
	Account findByUsersAndTypeAndName(User user, AccountType type, String accName);
}
