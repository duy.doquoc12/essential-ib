package essential_ib.repositories;

import essential_ib.entities.Person;
import org.springframework.data.repository.CrudRepository;

public interface PersonRepository extends CrudRepository<Person, Long> {

	/**
	 * Select * from person where firstname=:firstName and lastname=:lastName;
	 *
	 * @param firstName first name
	 * @param lastName last name
	 * @return person with first name and last name or null, if not exists
	 */
	Person findByFirstNameAndLastName(String firstName, String lastName);
}
