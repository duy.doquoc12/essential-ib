package essential_ib.repositories;

import essential_ib.entities.Transfer;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.Date;
import java.util.List;

public interface TransferRepository extends CrudRepository<Transfer, Long> {

	@Query(value = "select t from Transfer t " +
				   "where (t.receiver.id = :accId or t.sender.id = :accId) and t.createdOn >= :startDate " +
				   "order by t.createdOn desc")
	List<Transfer> findAllStartAtDate(Long accId, Date startDate);

	@Query(value = "select t from Transfer t " +
				   "where (t.receiver.id = :accId or t.sender.id = :accId) " +
				   "and t.createdOn >= :startDate and t.createdOn < :endDate " +
				   "order by t.createdOn desc")
	List<Transfer> findAllBetweenDates(Long accId, Date startDate, Date endDate);

	@Query(value = "select t from Transfer t " +
				   "join Account a ON a = t.sender " +
				   "join a.users u " +
				   "where u.id = :userId and t.saved = true " +
				   "order by t.createdOn desc")
	List<Transfer> findSavedTransfersOfUser(Long userId);
}
