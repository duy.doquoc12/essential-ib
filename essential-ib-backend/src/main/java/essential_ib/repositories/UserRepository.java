package essential_ib.repositories;

import essential_ib.entities.User;
import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<User, Long> {

	/**
	 * Select * from user where username=:userName;
	 *
	 * @param userName user name
	 * @return user with user name; or null, if not exists
	 */
	User findByUserName(String userName);

	/**
	 * Select * from user where username=:userName and password=:password;
	 *
	 * @param userName user name
	 * @param password password
	 * @return user with user name and password; or null, if not exists
	 */
	User findByUserNameAndPassword(String userName, String password);
}
