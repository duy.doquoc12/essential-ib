package essential_ib.services_impl;

import essential_ib.EssentialUtils;
import essential_ib.entities.Account;
import essential_ib.entities.User;
import essential_ib.enums.AccountType;
import essential_ib.repositories.AccountRepository;
import essential_ib.services.AccountService;
import essential_ib.services.UserService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class AccountServiceImpl implements AccountService {
	private static final Logger LOG = LogManager.getLogger(AccountServiceImpl.class);

	private final AccountRepository accountRepository;
	private final UserService userService;

	public AccountServiceImpl(AccountRepository accountRepository, UserService userService) {
		this.accountRepository = accountRepository;
		this.userService = userService;
	}

	@Override
	public void persist(Account account) {
		Account help = findByIban(account.getIban());
		String logMessage;
		if (help == null) {
			logMessage = "New account with id %d was created";
		} else {
			account.setId(help.getId());
			logMessage = "Account with id %d was updated";
		}

		if (account.getName() == null) {
			account.setName(account.getTypeValue());
		}

		account.setBalance(EssentialUtils.round(account.getBalance()));
		accountRepository.save(account);
		LOG.info(String.format(logMessage, account.getId()));
	}

	@Override
	public Account findByIban(String iban) {
		return accountRepository.findByIban(iban);
	}

	@Override
	public List<Account> findByUserId(Long userId) {
		User user = userService.findById(userId);
		return accountRepository.findByUsers(user);
	}

	@Override
	public Account findByUserIdAndTypeAndName(Long userId, AccountType type, String accName) {
		User user = userService.findById(userId);
		return accountRepository.findByUsersAndTypeAndName(user, type, accName);
	}
}
