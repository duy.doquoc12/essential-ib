package essential_ib.services_impl;

import essential_ib.entities.Address;
import essential_ib.repositories.AddressRepository;
import essential_ib.services.AddressService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class AddressServiceImpl implements AddressService {
	private static final Logger LOG = LogManager.getLogger(AddressServiceImpl.class);

	private final AddressRepository addressRepository;

	public AddressServiceImpl(AddressRepository addressRepository) {
		this.addressRepository = addressRepository;
	}

	@Override
	public Address persist(Address address) {
		String street = address.getStreet();
		String houseNumber = address.getHouseNumber();
		String zipCode = address.getZipCode();
		String place = address.getPlace();
		String country = address.getCountry();
		String stair = address.getStair();
		String doorNumber = address.getDoorNumber();

		if (find(street, houseNumber, zipCode, place, country, stair, doorNumber).isEmpty()) {
			address = addressRepository.save(address);
			LOG.info("New address with id {} was inserted", address.getId());
		} else {
			address = addressRepository.save(address);
			LOG.info("Address with id {} was updated", address.getId());
		}

		return address;
	}

	@Override
	public List<Address> find(String street, String houseNumber, String zipCode, String place, String country,
			String stair, String doorNumber) {
		return addressRepository
				.findByStreetAndHouseNumberAndZipCodeAndPlaceAndCountryAndStairAndDoorNumber(street, houseNumber,
						zipCode, place, country, stair, doorNumber);
	}
}
