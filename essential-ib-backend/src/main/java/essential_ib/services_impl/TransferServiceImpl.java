package essential_ib.services_impl;

import essential_ib.EssentialUtils;
import essential_ib.entities.Account;
import essential_ib.entities.Transfer;
import essential_ib.enums.Period;
import essential_ib.repositories.TransferRepository;
import essential_ib.services.AccountService;
import essential_ib.services.TransferService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Component
public class TransferServiceImpl implements TransferService {
	private static final Logger LOG = LogManager.getLogger(TransferServiceImpl.class);

	private final TransferRepository transferRepository;
	private final AccountService accountService;

	public TransferServiceImpl(TransferRepository transferRepository, AccountService accountService) {
		this.transferRepository = transferRepository;
		this.accountService = accountService;
	}

	@Override
	public Transfer persist(Transfer transfer) {
		return transferRepository.save(transfer);
	}

	@Override
	public void execute(Transfer transfer) {
		// update sender's and receiver's account balance
		Account sender = accountService.findByIban(transfer.getSender().getIban());
		Account receiver = accountService.findByIban(transfer.getReceiver().getIban());

		double amount = transfer.getAmount();
		sender.setBalance(sender.getBalance() - amount);
		receiver.setBalance(receiver.getBalance() + amount);

		transfer.getSender().getSendingTransfers().add(transfer);
		transfer.getReceiver().getReceivingTransfers().add(transfer);

		accountService.persist(sender);
		accountService.persist(receiver);

		LOG.info("Transfer {} successfully executed", transfer.getId());
	}

	@Override
	public List<Transfer> findTransferListOfAccountForPeriod(Long accId, Period period) {
		Date startDate, endDate;
		switch (period) {
		case LAST_MONTH:
			startDate = EssentialUtils.getFirstDate(Period.LAST_MONTH);
			endDate = EssentialUtils.getFirstDate(Period.THIS_MONTH);
			return transferRepository.findAllBetweenDates(accId, startDate, endDate);

		case LAST_QUARTER:
			startDate = EssentialUtils.getFirstDate(Period.LAST_QUARTER);
			endDate = EssentialUtils.getFirstDate(Period.THIS_QUARTER);
			return transferRepository.findAllBetweenDates(accId, startDate, endDate);

		default:
			startDate = EssentialUtils.getFirstDate(period);
			return transferRepository.findAllStartAtDate(accId, startDate);
		}
	}

	@Override
	public List<Transfer> getSavedTransfersOfUser(Long userId) {
		return transferRepository.findSavedTransfersOfUser(userId);
	}

	@Override
	public Transfer findById(Long id) {
		return transferRepository.findById(id).orElse(null);
	}

	@Override
	public void deleteTransfer(Transfer transfer) {
		transferRepository.delete(transfer);
		LOG.info("Transfer with id={} was deleted", transfer.getId());
	}
}
