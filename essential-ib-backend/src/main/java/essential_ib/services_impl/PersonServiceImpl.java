package essential_ib.services_impl;

import essential_ib.entities.Person;
import essential_ib.repositories.PersonRepository;
import essential_ib.services.PersonService;
import essential_ib.services.UserService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;

@Component
public class PersonServiceImpl implements PersonService {
	private static final Logger LOG = LogManager.getLogger(PersonServiceImpl.class);

	private final PersonRepository personRepository;
	private final UserService userService;

	public PersonServiceImpl(PersonRepository personRepository, UserService userService) {
		this.personRepository = personRepository;
		this.userService = userService;
	}

	@Override
	public void persist(Person person) {
		Person help = (Person) userService.findByUserName(person.getUserName());

		if (help == null) {
			help = findByFirstNameAndLastName(person.getFirstName(), person.getLastName());
		}

		if (help == null) {
			personRepository.save(person);
			LOG.info("New person with id {} was inserted", person.getId());
		} else {
			person.setId(help.getId());
			personRepository.save(person);
			LOG.info("Person with id {} was updated", person.getId());
		}
	}

	@Override
	public Person findByFirstNameAndLastName(String firstName, String lastName) {
		return personRepository.findByFirstNameAndLastName(firstName, lastName);
	}
}
