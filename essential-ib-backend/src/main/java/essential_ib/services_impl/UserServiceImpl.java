package essential_ib.services_impl;

import essential_ib.entities.User;
import essential_ib.repositories.UserRepository;
import essential_ib.services.UserService;
import org.springframework.stereotype.Component;

@Component
public class UserServiceImpl implements UserService {

	private final UserRepository userRepository;

	public UserServiceImpl(UserRepository userRepository) {
		this.userRepository = userRepository;
	}

	@Override
	public User findByUserName(String userName) {
		return userRepository.findByUserName(userName);
	}

	@Override
	public User findByUserNameAndPassword(String userName, String password) {
		return userRepository.findByUserNameAndPassword(userName, password);
	}

	@Override
	public User findById(Long id) {
		return userRepository.findById(id).orElse(null);
	}
}
