package essential_ib;

import essential_ib.enums.Period;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;

public class EssentialUtils {

	/**
	 * Round any given number to 2 decimal point.
	 */
	public static Double round(Double num) {
		BigDecimal hundred = BigDecimal.valueOf(100);
		BigDecimal tmp = hundred.multiply(BigDecimal.valueOf(num));
		return Math.round(tmp.doubleValue()) / 100.0;
	}

	/**
	 * Calculate the first day of a given {@link Period}.
	 *
	 * @param period period
	 * @return first day
	 */
	public static Date getFirstDate(Period period) {
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.HOUR, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.DAY_OF_MONTH, 1);

		switch (period) {
		case LAST_MONTH:
			cal.set(Calendar.MONTH, cal.get(Calendar.MONTH) - 1);
			break;

		case THIS_QUARTER:
			cal.set(Calendar.MONTH, cal.get(Calendar.MONTH) / 3 * 3);
			break;

		case LAST_QUARTER:
			cal.set(Calendar.MONTH, cal.get(Calendar.MONTH) / 6 * 6);
			break;

		case HALF_YEAR:
			cal.set(Calendar.MONTH, cal.get(Calendar.MONTH) - 6);
			break;

		default:
			break;
		}

		return cal.getTime();
	}
}
