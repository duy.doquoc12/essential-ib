package essential_ib.entities;

import essential_ib.enums.AccountType;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Account extends BaseEntity {

	@Enumerated(EnumType.STRING)
	private AccountType type;

	private String iban;
	private String name; // Accountname
	private double balance; // Kontostand

	@ManyToMany(mappedBy = "accounts")
	private List<User> users = new ArrayList<>();

	@OneToMany(mappedBy = "sender")
	private List<Transfer> sendingTransfers = new ArrayList<>();

	@OneToMany(mappedBy = "receiver")
	private List<Transfer> receivingTransfers = new ArrayList<>();

	public Account() {
	}

	public String getIban() {
		return iban;
	}

	public void setIban(String iban) {
		this.iban = iban;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getBalance() {
		return balance;
	}

	public void setBalance(double balance) {
		this.balance = balance;
	}

	public List<User> getUsers() {
		return users;
	}

	public void setUsers(List<User> users) {
		this.users = users;
	}

	public AccountType getType() {
		return type;
	}

	/**
	 * Get account type value. <br>
	 * Implemented for AccountToDtoMapper
	 *
	 * @return account type value
	 */
	public String getTypeValue() {
		return type.getValue();
	}

	public void setType(AccountType type) {
		this.type = type;
	}

	/**
	 * Set account type by its value. <br>
	 * Implemented for AccountMapper
	 *
	 * @param value account type value
	 */
	public void setTypeByValue(String value) {
		try {
			this.type = AccountType.findByString(value);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public List<Transfer> getSendingTransfers() {
		return sendingTransfers;
	}

	public void setSendingTransfers(List<Transfer> sendingTransfers) {
		this.sendingTransfers = sendingTransfers;
	}

	public List<Transfer> getReceivingTransfers() {
		return receivingTransfers;
	}

	public void setReceivingTransfers(List<Transfer> receivingTransfers) {
		this.receivingTransfers = receivingTransfers;
	}
}
