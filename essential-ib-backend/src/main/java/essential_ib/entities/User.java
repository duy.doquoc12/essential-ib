package essential_ib.entities;


import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity(name = "IBUSER")
@Inheritance(strategy = InheritanceType.JOINED)
public abstract class User extends BaseEntity {

	@Column(name = "USERNAME")
	private String userName;

	private String password;

	@Column(name = "TELNUMBER")
	private String telNumber;

	private String email;

	@ManyToMany
	@JoinTable(name = "USER_ACCOUNT",
			joinColumns = @JoinColumn(name = "USER_ID"),
			inverseJoinColumns = @JoinColumn(name = "ACCOUNT_ID"))
	private List<Account> accounts = new ArrayList<>();

	@ManyToMany
	@JoinTable(name = "USER_ADDRESS",
			joinColumns = @JoinColumn(name = "USER_ID"),
			inverseJoinColumns = @JoinColumn(name = "ADDRESS_ID"))
	private List<Address> addressList = new ArrayList<>();

	public User() {
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String username) {
		this.userName = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getTelNumber() {
		return telNumber;
	}

	public void setTelNumber(String telNumber) {
		this.telNumber = telNumber;
	}

	public List<Account> getAccounts() {
		return accounts;
	}

	public void setAccounts(List<Account> accounts) {
		this.accounts = accounts;
	}

	public void addAccount(Account account) {
		accounts.add(account);
	}

	public List<Address> getAddress() {
		return addressList;
	}

	public void setAddressList(List<Address> addressList) {
		this.addressList = addressList;
	}

	public void addAddress(Address address) {
		addressList.add(address);
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
}
