package essential_ib.entities;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import java.util.Date;

@MappedSuperclass
public abstract class BaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(updatable = false, nullable = false)
    private Long id;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "CREATEDON", nullable = false, updatable = false)
    @CreatedDate
    private Date createdOn;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "UPDATEDON", nullable = false)
    @LastModifiedDate
    private Date updatedOn;

    public BaseEntity() {}

    public Long getId() { return id; }

    public void setId(Long id) { this.id = id; }

    public Date getUpdatedOn() { return updatedOn; }

    public void setUpdatedOn(Date updatedOn) {
        this.updatedOn = updatedOn;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    @PrePersist
    @SuppressWarnings("unused")
    protected void prePersist() {
        if (this.createdOn == null) { createdOn = new Date(); }
        if (this.updatedOn == null) { updatedOn = new Date(); }
    }

    @PreUpdate
    @SuppressWarnings("unused")
    protected void preUpdate() { this.updatedOn = new Date(); }

    @PreRemove
    @SuppressWarnings("unused")
    protected void preRemove() { this.updatedOn = new Date(); }
}
