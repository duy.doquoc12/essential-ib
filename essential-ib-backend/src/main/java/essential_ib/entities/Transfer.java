package essential_ib.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Transfer extends BaseEntity {

	@ManyToOne
	@JoinColumn(name = "SENDER_ID")
	private Account sender;

	@ManyToOne
	@JoinColumn(name = "RECEIVER_ID")
	private Account receiver;

	@Column(name = "RECEIVERNAME")
	private String receiverName;

	@Column(name = "USAGESTR")
	private String usage;

	private double amount;
	private Boolean saved = false;

	public Transfer(){
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public Account getSender() {
		return sender;
	}

	public void setSender(Account sender) {
		this.sender = sender;
	}

	public Account getReceiver() {
		return receiver;
	}

	public void setReceiver(Account receiver) {
		this.receiver = receiver;
	}

	public String getReceiverName() {
		return receiverName;
	}

	public void setReceiverName(String receiverName) {
		this.receiverName = receiverName;
	}

	public String getUsage() {
		return usage;
	}

	public void setUsage(String usage) {
		this.usage = usage;
	}

	public Boolean isSaved() {
		return saved;
	}

	public void setSaved(Boolean saved) {
		this.saved = saved;
	}
}
