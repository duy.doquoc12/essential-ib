package essential_ib.entities;

import javax.persistence.Entity;

@Entity
public class Organization extends User {

	private String name;
	private String uid;

	public Organization() {
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUid() {
		return uid;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}
}
