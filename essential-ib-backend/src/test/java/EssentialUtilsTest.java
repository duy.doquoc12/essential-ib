import essential_ib.EssentialUtils;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class EssentialUtilsTest {

	@Test
	void round() {
		assertEquals(1.23, EssentialUtils.round(1.234));
		assertEquals(1.24, EssentialUtils.round(1.235));
		assertEquals(-1.23, EssentialUtils.round(-1.234));
		assertEquals(-1.23, EssentialUtils.round(-1.235));
		assertEquals(-1.24, EssentialUtils.round(-1.236));
	}
}