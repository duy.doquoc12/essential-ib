export const environment = {
  production: true,
  apiUrl: 'https://essential-ib-backend.herokuapp.com/api'
};
