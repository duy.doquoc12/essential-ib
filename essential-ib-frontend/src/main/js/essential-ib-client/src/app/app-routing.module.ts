import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {DashboardComponent} from './dashboard/dashboard.component';
import {CheckingAccountComponent} from './accounts/checking-account/checking-account.component';
import {LoginComponent} from './login';
import {AuthGuard} from './_helpers';
import {SavingAccountComponent} from './accounts/saving-account/saving-account.component';
import {SignComponent} from './sign/sign.component';

const routes: Routes = [
  {path: '', redirectTo: '/dashboard', pathMatch: 'full'},
  {path: 'login', component: LoginComponent, pathMatch: 'full'},
  {path: 'dashboard', component: DashboardComponent, canActivate: [AuthGuard]},
  {path: 'accounts/checking/:name', component: CheckingAccountComponent, canActivate: [AuthGuard]},
  {path: 'accounts/saving/:name', component: SavingAccountComponent, canActivate: [AuthGuard]},
  {path: 'sign', component: SignComponent, canActivate: [AuthGuard]},

  // otherwise redirect to dashboard
  {path: '**', redirectTo: ''},
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(routes),
  ],
  exports: [
    RouterModule,
  ],
  declarations: [],
})
export class RoutingModule {
}
