import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MaterialModule} from './_material/material.module';
import {LayoutComponent} from './_layout/layout.component';
import {FlexLayoutModule} from '@angular/flex-layout';
import {RoutingModule} from './app-routing.module';
import {HeaderComponent} from './header/header.component';
import {SettingsComponent} from './settings/settings.component';
import {SidenavListComponent} from './header/sidenav-list/sidenav-list.component';
import {DashboardComponent} from './dashboard/dashboard.component';
import {AccountsComponent, CheckingAccountComponent, SavingAccountComponent} from './accounts';
import {LoginComponent} from './login';
import {ReactiveFormsModule} from '@angular/forms';
import {BasicAuthInterceptor, ErrorInterceptor} from './_helpers';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {TransferComponent, TransferConfirmationComponent, TransferDetailsComponent} from './_dialogs';
import {SignComponent} from './sign/sign.component';
import {AlertModule} from './_alert';
import {TabOnInputDirective} from './_directives/tab-on-input.directive';

@NgModule({
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MaterialModule,
    FlexLayoutModule,
    RoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    AlertModule,
  ],
  providers: [
    {provide: HTTP_INTERCEPTORS, useClass: BasicAuthInterceptor, multi: true},
    {provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true},
  ],
  declarations: [
    AppComponent,
    LayoutComponent,
    HeaderComponent,
    SettingsComponent,
    SidenavListComponent,
    DashboardComponent,
    AccountsComponent,
    CheckingAccountComponent,
    LoginComponent,
    TransferComponent,
    SavingAccountComponent,
    TransferDetailsComponent,
    TransferConfirmationComponent,
    SignComponent,
    TabOnInputDirective,
  ],
  bootstrap: [AppComponent],
})
export class AppModule {
}
