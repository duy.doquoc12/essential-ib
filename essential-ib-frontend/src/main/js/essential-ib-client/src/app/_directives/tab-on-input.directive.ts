import {Directive, HostListener, Input} from '@angular/core';

@Directive({
  selector: '[next-tab], [prev-tab]',
})
export class TabOnInputDirective {

  @Input('next-tab') nextControl: any;
  @Input('prev-tab') prevControl: any;

  @HostListener('keyup', ['$event'])
  onKeyUp(event: KeyboardEvent) {
    const numbers = /[0-9]/;

    // focus next field after a number or right arrow key was pressed
    if (numbers.test(event.key) || event.key === 'ArrowRight') {
      if (this.nextControl) {
        if (this.nextControl.focus) {
          this.nextControl.focus();
          this.nextControl.select();
          event.preventDefault();
          return false;
        }
      }
    } else if (event.key === 'ArrowLeft') {
      // focus previous field after left arrow key was pressed
      if (this.prevControl) {
        if (this.prevControl.focus) {
          this.prevControl.focus();
          this.prevControl.select();
          event.preventDefault();
          return false;
        }
      }
    }
  }

  constructor() {
  }

}
