import {Component, OnInit} from '@angular/core';
import {AccountService, AuthenticationService} from '../../_services';
import {Router} from '@angular/router';
import {Account} from '../../_models';

@Component({
  selector: 'app-saving-account',
  templateUrl: './saving-account.component.html',
  styleUrls: ['./saving-account.component.css'],
})
export class SavingAccountComponent implements OnInit {
  name: string;
  type: string;
  iban: string;

  balance: string;
  isPositive: boolean;

  selectedPeriod = 'THIS_MONTH';
  totalIncome: string;
  totalExpenditure: string;

  constructor(private authenticationService: AuthenticationService, private router: Router, private accountService: AccountService) {
  }

  ngOnInit(): void {
    const urlInfos = this.router.url.split('/');

    // set acc name, acc id, acc type and acc balance for current user
    const user = this.authenticationService.userValue;
    this.accountService.getAccountByTypeAndName(user.id, urlInfos[2], urlInfos[3]).subscribe((account: Account) => {
      this.name = (account.name !== null) ? account.name : account.type;
      this.type = account.type;
      this.iban = account.iban;
      this.setBalance(account);
      this.setAccountHistory(account);
    }, () => {
      this.router.navigate(['dashboard']);
    });
  }

  private setBalance(account: Account) {
    const balance = account.balance;

    // get sign
    let sign = '';
    if (balance >= 0) {
      sign = '+';
      this.isPositive = true;
    } else {
      this.isPositive = false;
    }

    // get correct number formatting
    const infos = balance.toString().split('.');

    let number = '';
    let pointCounter = 0;
    for (let i = infos[0].length - 1; i >= 0; i--) {
      number = infos[0].charAt(i) + number;
      pointCounter++;
      if (pointCounter === 3 && infos[0].charAt(i) !== '-') {
        number = '.' + number;
        pointCounter = 0;
      }
    }

    const decimalPlace = (infos[1] !== undefined) ? infos[1].toString() : '00';

    this.balance = sign + number + ',' + decimalPlace;
  }

  private setAccountHistory(account: Account) {
    this.totalIncome = '+1.045,00';
    this.totalExpenditure = '-97,85';
  };
}
