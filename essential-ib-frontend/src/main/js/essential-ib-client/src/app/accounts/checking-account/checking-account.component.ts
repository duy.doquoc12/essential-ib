import {Component, OnInit} from '@angular/core';
import {AccountService, AuthenticationService, TransferService} from '../../_services';
import {Account, Transfer} from '../../_models';
import {Router} from '@angular/router';
import {MatDialog} from '@angular/material/dialog';
import {TransferDetailsComponent} from '../../_dialogs';
import {Utils} from '../../_helpers';

@Component({
  selector: 'app-checking-account',
  templateUrl: './checking-account.component.html',
  styleUrls: ['./checking-account.component.css'],
})
export class CheckingAccountComponent implements OnInit {
  accId: number;
  name: string;
  type: string;
  iban: string;

  balance: string;
  isPositive: boolean;

  selectedPeriod = 'THIS_MONTH';
  totalIncome: string;
  totalExpenditure: string;

  transferList: Transfer[];

  constructor(private authenticationService: AuthenticationService, private router: Router,
    private accountService: AccountService, private transferService: TransferService, private dialog: MatDialog) {
  }

  ngOnInit(): void {
    const urlInfos = this.router.url.split('/');

    // set acc name, acc id, acc type and acc balance for current user
    const user = this.authenticationService.userValue;
    this.accountService.getAccountByTypeAndName(user.id, urlInfos[2], urlInfos[3]).subscribe((account: Account) => {
      this.accId = account.id;
      this.name = account.name;
      this.type = account.type;
      this.iban = account.iban;
      this.setBalance(account);

      this.setTransferDetails();
    }, () => {
      this.router.navigate(['dashboard']);
    });
  }

  setTransferDetails() {
    this.transferService.getTransferListForAccount(this.accId, this.selectedPeriod).subscribe((data: Transfer[]) => {
      this.transferList = data;
      this.setTotal();
    });
  }

  setShortDescription(transfer: Transfer) {
    if (transfer.usage !== null) {
      return transfer.usage;
    }

    if (transfer.sender.id === this.accId) {
      if (transfer.receiverName !== null) {
        return 'Überwiesen an ' + transfer.receiverName;
      } else {
        return 'Überwiesen an ' + transfer.receiver.iban;
      }
    } else {
      return 'Empfangen von ' + transfer.sender.iban;
    }
  }

  setIncome(transfer: Transfer) {
    if (transfer.receiver.id === this.accId) {
      return Utils.formatMoney(transfer.amount) + ' EUR';
    }
  }

  setExpenditure(transfer: Transfer) {
    if (transfer.sender.id === this.accId) {
      return Utils.formatMoney(-1 * transfer.amount) + ' EUR';
    }
  }

  openTransfer(transfer: Transfer) {
    this.dialog.open(TransferDetailsComponent, {
      data: {transfer: transfer, accountName: this.name, iban: this.iban},
    });
  }

  private setBalance(account: Account) {
    this.isPositive = account.balance >= 0;
    this.balance = Utils.formatMoney(account.balance);
  }

  /**
   * Set total income and expenditure of selected period.
   */
  private setTotal() {
    let totalIncome = 0;
    let totalExpenditure = 0;
    for (let i = 0; i < this.transferList.length; i++) {
      const transfer = this.transferList[i];

      if (transfer.receiver.id === this.accId) {
        totalIncome += transfer.amount;
      } else {
        totalExpenditure -= transfer.amount;
      }
    }

    this.totalIncome = Utils.formatMoney(totalIncome);
    this.totalExpenditure = Utils.formatMoney(totalExpenditure);
  }
}
