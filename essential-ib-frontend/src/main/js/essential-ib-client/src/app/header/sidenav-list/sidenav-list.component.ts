import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {AuthenticationService} from '../../_services';
import {Router} from '@angular/router';
import {User} from '../../_models';
import {Observable} from 'rxjs';
import {MatDialog} from '@angular/material/dialog';
import {TransferComponent} from '../../_dialogs';

@Component({
  selector: 'app-sidenav-list',
  templateUrl: './sidenav-list.component.html',
  styleUrls: ['./sidenav-list.component.css'],
})
export class SidenavListComponent implements OnInit {
  user: Observable<User>;
  @Output() sidenavClose = new EventEmitter();

  constructor(private router: Router, private authenticationService: AuthenticationService, private dialog: MatDialog) {
    this.user = this.authenticationService.user;
  }

  ngOnInit(): void {
  }

  openNewTransfer() {
    this.dialog.open(TransferComponent);
  }

  onSidenavClose() {
    this.sidenavClose.emit();
  }

  logout() {
    this.onSidenavClose();
    this.authenticationService.logout();
  }
}
