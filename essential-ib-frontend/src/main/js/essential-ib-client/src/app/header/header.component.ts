import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {AuthenticationService, PersonService} from '../_services';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css'],
})
export class HeaderComponent implements OnInit {
  @Output() public sidenavToggle = new EventEmitter();

  name: string;

  constructor(private authenticationService: AuthenticationService, private personService: PersonService) {
  }

  ngOnInit(): void {
    const user = this.authenticationService.userValue;
    this.personService.getFullNameById(user.id).subscribe((person: Map<string, string>) => {
      this.name = person['firstName'] + ' ' + person['lastName'];
    });
  }

  onToggleSidenav() {
    this.sidenavToggle.emit();
  }
}
