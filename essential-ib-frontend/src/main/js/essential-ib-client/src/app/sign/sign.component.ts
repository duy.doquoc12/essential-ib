import {Component, OnInit} from '@angular/core';
import {AuthenticationService, TransferService} from '../_services';
import {Transfer} from '../_models';
import {Utils} from '../_helpers';

@Component({
  selector: 'app-sign',
  templateUrl: './sign.component.html',
  styleUrls: ['./sign.component.css'],
})
export class SignComponent implements OnInit {
  transfers: Transfer[];

  constructor(private transferService: TransferService, private authenticationService: AuthenticationService) {
  }

  ngOnInit(): void {
    this.transferService.getTransferSavedList(this.authenticationService.userValue.id).subscribe((data: Transfer[]) => {
      this.transferService.setSavedTransfers(data);

      this.transferService.savedTransfers.subscribe((data: Transfer[]) => {
        this.transfers = data;
      });
    });
  }

  deleteTransfer(transfer: Transfer) {
    this.transferService.deleteTransfer(transfer.id).subscribe();
    this.transferService.spliceSavedTransfer(transfer);
  }

  signALL() {
    this.transferService.executeSavedTransfers(this.authenticationService.userValue.id).subscribe(() => {
      this.transfers = [];
    });
  }

  formatMoney(amount: number) {
    return Utils.formatMoneyWithoutSign(amount);
  }
}
