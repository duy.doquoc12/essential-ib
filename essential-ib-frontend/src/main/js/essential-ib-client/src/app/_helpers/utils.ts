import {Injectable} from '@angular/core';

@Injectable()
export class Utils {
  constructor() {
  }

  public static formatMoneyWithoutSign(balance: number) {
    // get correct number formatting
    const infos = balance.toString().split('.');

    // format integer left of the decimal point
    let number = '';
    let pointCounter = 0;
    for (let i = infos[0].length - 1; i >= 0; i--) {
      const char = infos[0].charAt(i);
      number = char + number;
      pointCounter++;
      if (pointCounter === 3 && i !== 0 && infos[0].charAt(i - 1) !== '-') {
        number = '.' + number;
        pointCounter = 0;
      }
    }

    // format decimal places
    let decimalPlace = (infos[1] !== undefined) ? infos[1] : '00';
    if (decimalPlace.length === 1) {
      decimalPlace = infos[1].toString() + '0';
    }

    return number + ',' + decimalPlace;
  }

  public static formatMoney(balance: number) {
    balance = Number(balance.toFixed(2));
    // get sign
    let sign = '';
    if (balance >= 0) {
      sign = '+';
    }

    return sign + Utils.formatMoneyWithoutSign(balance);
  }
}
