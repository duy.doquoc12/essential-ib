import {Component, OnInit} from '@angular/core';
import {AccountService, AuthenticationService} from '../_services';
import {Account} from '../_models';
import {Router} from '@angular/router';
import {MatDialog} from '@angular/material/dialog';
import {TransferComponent} from '../_dialogs';
import {Utils} from '../_helpers';
import {Observable} from 'rxjs';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css'],
})
export class DashboardComponent implements OnInit {
  accounts$: Observable<Account[]>;

  constructor(private authenticationService: AuthenticationService, private accountService: AccountService,
    private router: Router, private dialog: MatDialog) {
  }

  ngOnInit(): void {
    const user = this.authenticationService.userValue;
    this.accounts$=this.accountService.getAccounts(user.id);
  }

  isPositive(account: Account): boolean {
    return account.balance >= 0;
  }

  getBalance(account: Account) {
    return Utils.formatMoney(account.balance);
  }

  navigateToAccount(account: Account) {
    let accType;
    if (account.type === 'Girokonto') {
      accType = 'checking';
    } else if (account.type === 'Sparkonto') {
      accType = 'saving';
    }

    if (accType !== null) {
      this.router.navigate(['/accounts/' + accType + '/' + account.name]);
    }
  }

  openTransferDialog(account: Account) {
    this.dialog.open(TransferComponent, {
      data: {iban: account.iban},
    });
  }
}
