﻿export class Account {
  id: number;
  iban: string;
  type: string;
  name: string;
  balance: number;
}
