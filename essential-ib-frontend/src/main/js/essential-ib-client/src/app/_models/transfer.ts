﻿import {Account} from './account';

export class Transfer {
  id: number;
  createdOnStr: string;
  sender: Account;
  receiverName: string;
  receiver: Account;
  usage: string;
  amount: number;
}
