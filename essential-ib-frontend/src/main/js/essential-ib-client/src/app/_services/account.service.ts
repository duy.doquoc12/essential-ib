import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Account} from '../_models';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root',
})
export class AccountService {
  constructor(private http: HttpClient) {
  }

  getAccounts(userId: number) {
    let params = new HttpParams();
    params = params.set('userId', String(userId));

    return this.http.get<Account[]>(`${environment.apiUrl}/accounts`, {params: params});
  }

  getAccountByTypeAndName(userId: number, type: string, name: string) {
    let params = new HttpParams();
    params = params.set('userId', String(userId));

    return this.http.get<Account>(`${environment.apiUrl}/accounts/account/${type}/${name}`, {params: params});
  }

  ibanExist(iban: string) {
    let params = new HttpParams();
    params = params.set('iban', iban);

    return this.http.get<boolean>(`${environment.apiUrl}/accounts/account/`, {params: params});
  }
}
