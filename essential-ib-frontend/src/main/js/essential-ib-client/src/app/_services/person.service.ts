import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root',
})
export class PersonService {
  constructor(private http: HttpClient) {
  }

  getFullNameById(id: number) {
    let params = new HttpParams();
    params = params.set('id', String(id));

    return this.http.get<Map<string, string>>(`${environment.apiUrl}/person/name`, {params: params});
  }
}
