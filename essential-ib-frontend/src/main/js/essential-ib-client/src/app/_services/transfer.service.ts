import {Injectable} from '@angular/core';
import {environment} from '../../environments/environment';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Transfer} from '../_models';
import {BehaviorSubject, Observable} from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class TransferService {
  private transferSubject = new BehaviorSubject<boolean>(false);
  transferSuccessful: Observable<boolean>;

  private savedSubject = new BehaviorSubject<Transfer[]>([]);
  savedTransfers: Observable<Transfer[]>;

  constructor(private http: HttpClient) {
    this.transferSuccessful = this.transferSubject.asObservable();
    this.savedTransfers = this.savedSubject.asObservable();
  }

  /**
   * Execute or save a transfer.
   *
   * @param senderIban iban of the sender
   * @param receiverName custom name of the receiver
   * @param receiverIban iban of the receiver
   * @param usage custom usage message
   * @param amount amount of money to transfer from sender to receiver
   * @param save determine whether the transfer should be saved in order to sign multiple transfers, <br>
   *   or executed immediately
   * @param id optional
   */
  transfer(senderIban: string, receiverName: string, receiverIban: string, usage: string, amount: number,
    save: boolean, id?: number) {
    return this.http.post<Transfer>(`${environment.apiUrl}/transfer`, {
      senderIban, receiverName, receiverIban, usage, amount, save, id,
    });
  }

  getTransferListForAccount(accId: number, period: string) {
    let params = new HttpParams();
    params = params.set('accId', String(accId));
    params = params.set('period', period);

    return this.http.get<Transfer[]>(`${environment.apiUrl}/transfer/transferList`, {params: params});
  }

  setTransferSuccessful(transferSuccessful: boolean) {
    this.transferSubject.next(transferSuccessful);
  }

  getTransferSavedList(userId: number) {
    let params = new HttpParams();
    params = params.set('userId', String(userId));
    return this.http.get<Transfer[]>(`${environment.apiUrl}/transfer/savedList`, {params: params});
  }

  deleteTransfer(transferId: number) {
    return this.http.post(`${environment.apiUrl}/transfer/delete`, {transferId});
  }

  setSavedTransfers(transfers: Transfer[]) {
    this.savedSubject.next(transfers);
  }

  addSavedTransfer(transfer: Transfer) {
    const transfers = this.savedSubject.getValue();
    transfers.push(transfer);
    this.savedSubject.next(transfers);
  }

  spliceSavedTransfer(transfer: Transfer) {
    const transfers = this.savedSubject.getValue();
    const index = transfers.indexOf(transfer);
    transfers.splice(index, 1);
  }

  executeSavedTransfers(userId: number) {
    return this.http.post(`${environment.apiUrl}/transfer/saved/execute/all`, {userId});
  }

}
