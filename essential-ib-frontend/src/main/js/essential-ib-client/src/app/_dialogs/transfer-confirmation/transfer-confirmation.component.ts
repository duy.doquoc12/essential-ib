import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material/dialog';
import {Utils} from '../../_helpers';
import {FormGroup} from '@angular/forms';
import {TransferService} from '../../_services';

@Component({
  selector: 'app-transfer-confirmation',
  templateUrl: './transfer-confirmation.component.html',
  styleUrls: ['./transfer-confirmation.component.css'],
})
export class TransferConfirmationComponent implements OnInit {
  receiverName: string;
  iban: string;
  amount: string;

  constructor(@Inject(MAT_DIALOG_DATA)
    public data: { selectedAccount: string, amount: number, transferForm: FormGroup },
    private transferService: TransferService) {
  }

  ngOnInit(): void {
    const transferForm = this.data.transferForm;
    this.receiverName = transferForm.value.receiverName;
    this.iban = transferForm.value.iban;

    this.amount = Utils.formatMoneyWithoutSign(this.data.amount);
  }

  send() {
    const amount = Number(this.amount.replace(',', '.'));

    this.transferService.transfer(this.data.selectedAccount, this.receiverName, this.iban,
      this.data.transferForm.value.usage, amount, false).subscribe((data) => {
      this.transferService.setTransferSuccessful(true);
    });
  }
}
