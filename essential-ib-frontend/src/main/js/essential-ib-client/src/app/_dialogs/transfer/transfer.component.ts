import {Component, Inject, OnInit} from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import {Account, Transfer} from '../../_models';
import {AccountService, AuthenticationService, TransferService} from '../../_services';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material/dialog';
import {TransferConfirmationComponent} from '../transfer-confirmation/transfer-confirmation.component';
import {Observable} from 'rxjs';
import {MatSnackBar} from '@angular/material/snack-bar';

@Component({
  selector: 'app-transfer',
  templateUrl: './transfer.component.html',
  styleUrls: ['./transfer.component.css'],
})
export class TransferComponent implements OnInit {
  accounts$: Observable<Account[]>
  selectedAccount: string; // iban
  amount: number;

  transferForm = new FormGroup({
    receiverName: new FormControl(''),
    iban: new FormControl(''),
    usage: new FormControl(''),
    dropdown: new FormControl(''),
  });

  amountForm = new FormGroup({
    hundredK: new FormControl(''),
    tenK: new FormControl(''),
    thousand: new FormControl(''),
    hundred: new FormControl(''),
    ten: new FormControl(''),
    one: new FormControl(''),
    decOne: new FormControl(''),
    decTen: new FormControl(''),
  })
  amountMissing = true;

  errorSender = false;
  errorIban = false;
  errorMessage: string;

  cancelCloseButton: string;
  transferSuccess = false;

  ibanExists$: Observable<boolean>;
  ibanExistsBool: boolean;

  options = {
    autoClose: true,
    keepAfterRouteChange: false,
  };

  constructor(private dialogRef: MatDialogRef<TransferComponent>, private authenticationService: AuthenticationService,
    private accountService: AccountService, @Inject(MAT_DIALOG_DATA) public data: { iban: string },
    private dialog: MatDialog, private transferService: TransferService, private snackBar: MatSnackBar) {
  }

  ngOnInit(): void {
    const user = this.authenticationService.userValue;
    this.accounts$ = this.accountService.getAccounts(user.id);

    this.cancelCloseButton = 'Abbrechen';

    this.accounts$ = this.accountService.getAccounts(user.id);
    this.transferForm.get('dropdown').setValue(this.data.iban);
    this.selectedAccount = this.transferForm.get('dropdown').value;
  }

  async openConfirmation() {
    // set false for errors - needed in case multiple transfers are made
    this.errorIban = false;
    this.errorSender = false;
    // set sender iban
    this.selectedAccount = this.transferForm.get('dropdown').value;

    if (!this.inputIsValid()) {
      return;
    }

    this.ibanExists$ = this.accountService.ibanExist(this.transferForm.value.iban);
    // set true / false if iban exists
    try {
      this.ibanExistsBool = await this.ibanExists$.toPromise();
    } catch (error) {
      this.ibanExistsBool = false;
    }

    if (this.ibanExistsBool) {
      this.transferService.transferSuccessful.subscribe((data) => {

        if (data) {
          this.cancelCloseButton = 'Schließen';
          this.transferSuccess = true;
          this.disableInput();
        }
      });
      this.dialog.open(TransferConfirmationComponent, {
        data: {
          selectedAccount: this.selectedAccount,
          amount: this.amount,
          transferForm: this.transferForm,
        },
      });
    } else {
      this.errorIban = true;
      this.errorMessage = 'Die IBAN des Empfängers existiert nicht.';
    }
  }

  /**
   * Transfer was successful, user choose to close the dialog.<br>
   * Action: close dialog and refresh the current page.
   */
  close() {
    if (this.transferSuccess) {
      window.location.reload();
    }
  }

  /**
   * Close this transfer and open a new one.
   */
  createNewTransfer() {
    this.dialogRef.close();

    this.dialog.open(TransferComponent, {
      data: {iban: this.selectedAccount},
    });
  }

  disableInput() {
    this.transferForm.disable();
  }

  async saveTransfer() {
    // set false for errors - needed in case multiple transfers are made
    this.errorIban = false;
    this.errorSender = false;
    // set sender iban
    this.selectedAccount = this.transferForm.get('dropdown').value;

    if (!this.inputIsValid()) {
      return;
    }

    this.ibanExists$ = this.accountService.ibanExist(this.transferForm.value.iban);
    // set true / false if iban exists
    try {
      this.ibanExistsBool = await this.ibanExists$.toPromise();
    } catch (error) {
      this.ibanExistsBool = false;
    }

    if (this.ibanExistsBool) {
      this.transferService.transfer(this.selectedAccount, this.transferForm.value.receiverName,
        this.transferForm.value.iban,
        this.transferForm.value.usage, this.amount, true).subscribe((data: Transfer) => {
        this.transferService.addSavedTransfer(data);
      });

      // message for transfer saving
      this.snackBar.open('Überweisung wurde gespeichert!', null, {
        duration: 2000,
      });

      // reset the input fields
      this.transferForm.reset();
      this.amountForm.reset();

      this.transferForm.get('dropdown').setValue(this.selectedAccount);
    } else {
      this.errorIban = true;
      this.errorMessage = 'Die IBAN des Empfängers existiert nicht.';
    }
  }

  formatNumber(event) {
    const field = this.amountForm.get(event.target.name);

    // validate input
    if (event.key === 'e' || event.key === 'E' || event.key === '+' || event.key === '-' || event.key === '.' ||
      event.key === ',' || event.key === 'Backspace' || event.key === 'ArrowUp' || event.key === 'ArrowDown') {
      field.setValue(null);
      return;
    }

    // display only one number max
    const numbers = /[0-9]/;
    if (numbers.test(event.key)) {
      this.amountForm.get(event.target.name).setValue(event.key);
      this.amountMissing = false;
    }
  }

  private inputIsValid(): boolean {
    // validate iban input
    if (this.selectedAccount === this.transferForm.value.iban) {
      this.errorSender = true;
      this.errorIban = true;
      this.errorMessage = 'Sender und Empfänger dürfen nicht identisch sein.';

      return false;
    } else if (!this.amountInputIsValid()) {
      this.amountMissing = true;
      this.errorMessage = 'Ein Betrag muss eingegeben werden.';

      return false;
    } else {
      this.errorSender = false;
      this.errorIban = false;
      this.amountMissing = false;

      return true;
    }
  }

  private amountInputIsValid(): boolean {
    let hundredK = this.amountForm.value.hundredK;
    let tenK = this.amountForm.value.tenK;
    let thousand = this.amountForm.value.thousand;
    let hundred = this.amountForm.value.hundred;
    let ten = this.amountForm.value.ten;
    let one = this.amountForm.value.one;
    let decOne = this.amountForm.value.decOne;
    let decTen = this.amountForm.value.decTen;

    if (this.isEmpty(hundredK) && this.isEmpty(tenK) && this.isEmpty(thousand) && this.isEmpty(hundred) &&
      this.isEmpty(ten) && this.isEmpty(one) && this.isEmpty(decOne) && this.isEmpty(decTen)) {
      return false;
    }

    // hundredK
    if (!this.isEmpty(hundredK)) {
      hundredK *= 100000;
    }

    // tenK
    if (!this.isEmpty(tenK)) {
      tenK *= 10000;
    } else if (!this.isEmpty(hundredK)) {
      tenK = 0;
      this.amountForm.get('tenK').setValue(0);
    }

    // thousand
    if (!this.isEmpty(thousand)) {
      thousand *= 1000;
    } else if (!this.isEmpty(tenK)) {
      thousand = 0;
      this.amountForm.get('thousand').setValue(0);
    }

    // hundred
    if (!this.isEmpty(hundred)) {
      hundred *= 100;
    } else if (!this.isEmpty(thousand)) {
      hundred = 0;
      this.amountForm.get('hundred').setValue(0);
    }

    // ten
    if (!this.isEmpty(ten)) {
      ten *= 10;
    } else if (!this.isEmpty(hundred)) {
      ten = 0;
      this.amountForm.get('ten').setValue(0);
    }

    // one - nothing special to consider

    // decOne
    if (!this.isEmpty(decOne)) {
      decOne /= 10;

      if (this.isEmpty(one)) {
        one = 0;
        this.amountForm.get('one').setValue(0);
      }
    } else {
      decOne = 0;
      this.amountForm.get('decOne').setValue(0);

      if (this.isEmpty(one)) {
        one = 0;
        this.amountForm.get('one').setValue(0);
      }
    }

    // decTen
    if (!this.isEmpty(decTen)) {
      decTen = decTen / 100;

      if (this.isEmpty(one)) {
        one = 0;
        this.amountForm.get('one').setValue(0);
      }
    } else {
      decTen = 0;
      this.amountForm.get('decTen').setValue(0);

      if (this.isEmpty(one)) {
        one = 0;
        this.amountForm.get('one').setValue(0);
      }
    }

    this.calculateAmount([hundredK, tenK, thousand, hundred, ten, one, decOne, decTen]);
    return true;
  }

  private calculateAmount(numbers: number[]) {
    let amount : number = 0;
    for (let i = 0; i < numbers.length; i++) {
      if (!this.isEmpty(numbers[i])) {
        amount += Number(numbers[i]);
      }
    }

    this.amount = Number(amount.toFixed(2));
  }

  private isEmpty(num: number): boolean {
    return num === null || num.toString().length === 0;
  }
}
