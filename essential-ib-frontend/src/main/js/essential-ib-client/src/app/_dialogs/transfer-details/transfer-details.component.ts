import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material/dialog';
import {Transfer} from '../../_models';
import {Utils} from '../../_helpers';

@Component({
  selector: 'app-transfer-details',
  templateUrl: './transfer-details.component.html',
  styleUrls: ['./transfer-details.component.css'],
})
export class TransferDetailsComponent implements OnInit {
  transfer: Transfer;
  accountName: string;
  accountIban: string;
  transferPartnerLabel: string;
  transferPartnerName: string;
  transferPartnerIban: string;
  amount: string

  constructor(@Inject(MAT_DIALOG_DATA) public data: { transfer: Transfer, accountName: string, iban: string }) {
  }

  ngOnInit(): void {
    this.transfer = this.data.transfer;

    this.accountName = this.data.accountName;
    this.accountIban = this.data.iban;

    // detail about a incoming transfer
    if (this.data.iban === this.transfer.receiver.iban) {
      this.transferPartnerLabel = 'Empfangen von';
      this.transferPartnerIban = this.transfer.sender.iban;
      this.amount = Utils.formatMoney(this.transfer.amount);
    } else {
      // detail about a outgoing transfer
      this.transferPartnerLabel = 'Überwiesen an';
      this.transferPartnerName = this.transfer.receiverName;
      this.transferPartnerIban = this.transfer.receiver.iban;
      this.amount = Utils.formatMoney(this.transfer.amount * -1);
    }
  }
}
