import {Component} from '@angular/core';
import {Router} from '@angular/router';

import {AuthenticationService} from './_services';
import {User} from './_models';
import {Observable} from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
   user$: Observable<User>

   constructor(private router: Router, private authenticationService: AuthenticationService) {
     this.user$ = this.authenticationService.user;
   }

   logout() {
     this.authenticationService.logout();
   }
}
