package essential_ib.controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import essential_ib.dtos.AccountDTO;
import essential_ib.facades.AccountFacade;
import io.swagger.v3.oas.annotations.Operation;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping({"/api/accounts"})
public class AccountController extends BaseController {
	private static final Logger LOG = LogManager.getLogger(AccountController.class);

	private final AccountFacade accountFacade;

	public AccountController(AccountFacade accountFacade) {
		this.accountFacade = accountFacade;
	}

	@Operation(summary = "Get all accounts of user")
	@GetMapping
	public ResponseEntity<List<AccountDTO>> getAccounts(
			@RequestParam(value = "userId") Long userId) throws JsonProcessingException {
		LOG.debug("Request data: " + formatToJson("userId", userId.toString()));

		List<AccountDTO> accountDTOs = accountFacade.findByUserId(userId);
		ResponseEntity<List<AccountDTO>> response = new ResponseEntity<>(accountDTOs, HttpStatus.OK);
		LOG.debug("Response data: " + mapper.writeValueAsString(response));
		return response;
	}

	@Operation(summary = "Try to find account of the user by its type and name")
	@GetMapping("/account/{type}/{name}")
	public ResponseEntity<AccountDTO> getAccountByTypeAndName(@RequestParam("userId") Long userId,
			@PathVariable String type, @PathVariable("name") String name) throws JsonProcessingException {
		LOG.debug("Request data: " + formatToJson("userId", userId.toString(), "accType", type, "accName", name));

		AccountDTO account = accountFacade.findByUserIdAndTypeAndName(userId, type, name);
		if (account != null) {
			ResponseEntity<AccountDTO> response = new ResponseEntity<>(account, HttpStatus.OK);
			LOG.debug("Response data: " + mapper.writeValueAsString(response));
			return response;
		} else {
			LOG.error("Couldn't find account of type={} and name={} for user with id:={}", type, name, userId);
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@Operation(summary = "Check whether account with given iban exist or not")
	@GetMapping("account")
	public ResponseEntity<Boolean> ibanExist(@RequestParam("iban") String iban) {
		LOG.debug("Request data: " + formatToJson("iban", iban));

		if (accountFacade.findByIban(iban) != null) {
			return new ResponseEntity<>(true, HttpStatus.OK);
		} else {
			LOG.error("Couldn't find account with iban={}", iban);
			return new ResponseEntity<>(false, HttpStatus.NOT_FOUND);
		}
	}
}
