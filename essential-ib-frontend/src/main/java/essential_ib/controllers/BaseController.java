package essential_ib.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;

public abstract class BaseController {

	@Autowired
	protected ObjectMapper mapper;

	/**
	 * Create string for logging in json format.
	 *
	 * @param variables variable name1, variable value1, name2, value2, ...
	 * @return string in json formatting
	 */
	protected String formatToJson(String... variables) {
		if (variables.length % 2 != 0) {
			throw new IllegalArgumentException("Variables has to contain pairs of variable name and its values");
		}

		StringBuilder str = new StringBuilder("{ ");
		for (int i = 0; i < variables.length; i++) {
			if (i % 2 == 1) {
				str.append("\"").append(variables[i - 1]).append("\": ");
				str.append("\"").append(variables[i]).append("\"");

				if (i != variables.length - 1) {
					str.append(", ");
				}
			}
		}

		str.append(" }");
		return str.toString();
	}
}
