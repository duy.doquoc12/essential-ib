package essential_ib.controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import essential_ib.dtos.UserDTO;
import essential_ib.facades.UserFacade;
import io.swagger.v3.oas.annotations.Operation;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
@RequestMapping({"/api/login"})
public class AuthenticationController extends BaseController {
	private static final Logger LOG = LogManager.getLogger(AuthenticationController.class);

	private final UserFacade userFacade;

	public AuthenticationController(UserFacade userFacade) {
		this.userFacade = userFacade;
	}

	@Operation(summary = "Try to find an user by user name and password. See wiki for example.")
	@PostMapping("/authenticate")
	public ResponseEntity<Long> authenticate(@RequestBody Map<String, String> dataMap) throws JsonProcessingException {
		String userName = dataMap.get("userName");
		String password = dataMap.get("password");

		dataMap.replace("password", "[PROTECTED]");
		LOG.debug("Request data: " + mapper.writeValueAsString(dataMap));

		UserDTO user = userFacade.findByUserNameAndPassword(userName, password);
		if (user != null) {
			ResponseEntity<Long> response = new ResponseEntity<>(user.getId(), HttpStatus.OK);
			LOG.debug("Response data: " + mapper.writeValueAsString(response));
			return response;
		} else {
			LOG.error("Authentication for user name={} and password=[PROTECTED] failed", userName);
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
}
