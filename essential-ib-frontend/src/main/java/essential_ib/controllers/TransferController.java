package essential_ib.controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import essential_ib.dtos.AccountDTO;
import essential_ib.dtos.TransferDTO;
import essential_ib.facades.AccountFacade;
import essential_ib.facades.TransferFacade;
import io.swagger.v3.oas.annotations.Operation;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.naming.OperationNotSupportedException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping({"/api/transfer"})
public class TransferController extends BaseController {
	private static final Logger LOG = LogManager.getLogger(TransferController.class);

	@Value("${config.profile}")
	private String currentProfile;

	private final ObjectMapper mapper;
	private final TransferFacade transferFacade;
	private final AccountFacade accountFacade;

	public TransferController(ObjectMapper mapper, TransferFacade transferFacade, AccountFacade accountFacade) {
		this.mapper = mapper;
		this.transferFacade = transferFacade;
		this.accountFacade = accountFacade;
	}

	@Operation(summary = "Transfer amount of money from sender account to receiver account. See Wiki for example.")
	@PostMapping
	public ResponseEntity<?> transfer(@RequestBody Map<String, String> dataMap) throws JsonProcessingException {
		LOG.debug("Request data: " + mapper.writeValueAsString(dataMap));

		String id = dataMap.get("id");
		String receiverName = dataMap.get("receiverName");
		String usage = dataMap.get("usage");
		boolean save = Boolean.parseBoolean(dataMap.get("save"));

		AccountDTO sender = accountFacade.findByIban(dataMap.get("senderIban"));
		AccountDTO receiver = accountFacade.findByIban(dataMap.get("receiverIban"));

		TransferDTO transfer = new TransferDTO();
		if (id != null) {
			transfer.setId(Long.parseLong(id));
		}
		transfer.setSender(sender);

		if (receiverName != null && receiverName.equals("")) {
			transfer.setReceiverName(null);
		} else {
			transfer.setReceiverName(receiverName);
		}

		transfer.setReceiver(receiver);

		if (usage != null && usage.equals("")) {
			transfer.setUsage(null);
		} else {
			transfer.setUsage(usage);
		}

		transfer.setAmount(Double.parseDouble(dataMap.get("amount")));
		transfer.setSaved(save);

		transfer = transferFacade.persist(transfer);
		if (!save) {
			transferFacade.execute(transfer);
		}

		LOG.debug("Respond data: " + mapper.writeValueAsString(transfer));
		return new ResponseEntity<>(transfer, HttpStatus.OK);
	}

	@Operation(summary = "Find all transfers in a certain time period of an account")
	@GetMapping("/transferList")
	public ResponseEntity<List<TransferDTO>> getTransferList(@RequestParam(value = "accId") Long accId,
			@RequestParam(value = "period") String period) throws JsonProcessingException,
			OperationNotSupportedException {
		LOG.debug("Request data: " + formatToJson("accId", accId.toString(), "period", period));

		List<TransferDTO> list = new ArrayList<>();
		for (TransferDTO transfer : transferFacade.findTransferListOfAccountForPeriod(accId, period)) {
			if (!transfer.isSaved()) {
				Calendar cal = Calendar.getInstance();
				cal.setTime(transfer.getCreatedOn());
				String date = getDayOfWeek(cal.get(Calendar.DAY_OF_WEEK)) + ". " +
							  cal.get(Calendar.DAY_OF_MONTH) + ". " +
							  (cal.get(Calendar.MONTH) + 1) + "."; // eg.: Di. 11. 4.
				transfer.setCreatedOnStr(date);

				list.add(transfer);
			}
		}

		ResponseEntity<List<TransferDTO>> respond = new ResponseEntity<>(list, HttpStatus.OK);
		LOG.debug("Respond data: " + mapper.writeValueAsString(respond));
		return respond;
	}

	@Operation(summary = "Find all saved transfers of an user")
	@GetMapping("/savedList")
	public ResponseEntity<List<TransferDTO>> getSavedList(
			@RequestParam(value = "userId") Long userId) throws JsonProcessingException {
		LOG.debug("Request data: " + formatToJson("userId", userId.toString()));

		List<TransferDTO> list = transferFacade.getSavedTransfersOfUser(userId);
		ResponseEntity<List<TransferDTO>> respond = new ResponseEntity<>(list, HttpStatus.OK);
		LOG.debug("Respond data: " + mapper.writeValueAsString(respond));
		return respond;
	}

	@Operation(summary = "Delete saved transfers of an user. See Wiki for example.")
	@PostMapping("/delete")
	public ResponseEntity<?> deleteTransfer(@RequestBody Map<String, Long> dataMap) throws JsonProcessingException {
		LOG.debug("Request data: " + mapper.writeValueAsString(dataMap));

		TransferDTO transfer = transferFacade.findById(dataMap.get("transferId"));
		if (transfer != null) {
			if (transfer.isSaved()) {
				transferFacade.deleteTransfer(transfer);
				return new ResponseEntity<>(HttpStatus.OK);
			} else {
				LOG.error("Deleting not saved transfer is currently forbidden");
				return new ResponseEntity<>(HttpStatus.FORBIDDEN);
			}
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@Operation(summary = "Execute all saved transfer of the user with given userId. See wiki for example.")
	@PostMapping("/saved/execute/all")
	public ResponseEntity<?> executeSavedTransfers(
			@RequestBody Map<String, Long> dataMap) throws JsonProcessingException {
		LOG.debug("Request data: " + mapper.writeValueAsString(dataMap));

		List<TransferDTO> transfers = transferFacade.getSavedTransfersOfUser(dataMap.get("userId"));
		for (int i = transfers.size() - 1; i >= 0; i--) {
			TransferDTO transfer = transfers.get(i);
			transfer.setSaved(false);

			transferFacade.persist(transfer);
			transferFacade.execute(transfer);
		}

		return new ResponseEntity<>(HttpStatus.OK);
	}

	private String getDayOfWeek(int day) throws OperationNotSupportedException {
		switch (day) {
		case 1:
			return "So";

		case 2:
			return "Mo";

		case 3:
			return "Di";

		case 4:
			return "Mi";

		case 5:
			return "Do";

		case 6:
			return "Fr";

		case 7:
			return "Sa";

		default:
			throw new OperationNotSupportedException();
		}
	}
}
