package essential_ib.controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import essential_ib.dtos.PersonDTO;
import essential_ib.facades.UserFacade;
import io.swagger.v3.oas.annotations.Operation;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping({"/api/person"})
public class PersonController extends BaseController {
	private static final Logger LOG = LogManager.getLogger(PersonController.class);

	private final UserFacade userFacade;

	public PersonController(UserFacade userFacade) {
		this.userFacade = userFacade;
	}

	@Operation(summary = "Get first and last name of user with given id")
	@GetMapping("/name")
	public ResponseEntity<Map<String, String>> getName(
			@RequestParam(value = "id") Long id) throws JsonProcessingException {
		LOG.debug("Request data: " + formatToJson("userId", String.valueOf(id)));

		PersonDTO person = (PersonDTO) userFacade.findById(id);
		if (person != null) {
			Map<String, String> map = new HashMap<>();
			map.put("firstName", person.getFirstName());
			map.put("lastName", person.getLastName());

			ResponseEntity<Map<String, String>> response = new ResponseEntity<>(map, HttpStatus.OK);
			LOG.debug("Response data: " + mapper.writeValueAsString(response));
			return response;
		} else {
			LOG.error("Couldn't find person with id={}", id);
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
}
