package essential_ib;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import essential_ib.dtos.AccountDTO;
import essential_ib.dtos.AddressDTO;
import essential_ib.dtos.PersonDTO;
import essential_ib.facades.AccountFacade;
import essential_ib.facades.AddressFacade;
import essential_ib.facades.PersonFacade;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Scanner;

@SpringBootApplication
public class PersonImporter {
	private static final Logger LOG = LogManager.getLogger(PersonImporter.class);

	public static void main(String[] args) throws JsonProcessingException {
		ConfigurableApplicationContext context = SpringApplication.run(PersonImporter.class, args);

		AccountFacade accountFacade = context.getBean(AccountFacade.class);
		AddressFacade addressFacade = context.getBean(AddressFacade.class);
		PersonFacade personFacade = context.getBean(PersonFacade.class);

		Scanner scanner =
				new Scanner(PersonImporter.class.getResourceAsStream("personImporter.json"), StandardCharsets.UTF_8);
		String json = scanner.useDelimiter("\\A").next();

		ObjectMapper objectMapper = new ObjectMapper();
		List<PersonDTO> people = objectMapper.readValue(json, new TypeReference<>() { });

		for (PersonDTO person : people) {
			// create accounts for person
			for (AccountDTO account : person.getAccounts()) {
				accountFacade.persist(account);
			}

			// create address list for person
			for (AddressDTO address : person.getAddressList()) {
				String street = address.getStreet();
				String houseNumber = address.getHouseNumber();
				String zipCode = address.getZipCode();
				String place = address.getPlace();
				String country = address.getCountry();
				String stair = address.getStair();
				String doorNumber = address.getDoorNumber();

				if (addressFacade.find(street, houseNumber, zipCode, place, country, stair, doorNumber).isEmpty()) {
					addressFacade.persist(address);
				}
			}

			personFacade.persist(person);
		}

		LOG.info("Import done");
		System.exit(0);
	}
}
