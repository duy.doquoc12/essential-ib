package essential_ib;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import essential_ib.dtos.AccountDTO;
import essential_ib.dtos.TransferDTO;
import essential_ib.enums.Period;
import essential_ib.facades.AccountFacade;
import essential_ib.facades.TransferFacade;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

import javax.transaction.NotSupportedException;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;

@SpringBootApplication
public class TransferImporter {
	private static final Logger LOG = LogManager.getLogger(TransferImporter.class);

	public static void main(String[] args) throws IOException, NotSupportedException {
		ConfigurableApplicationContext context = SpringApplication.run(PersonImporter.class, args);

		TransferFacade transferFacade = context.getBean(TransferFacade.class);
		AccountFacade accountFacade = context.getBean(AccountFacade.class);

		ObjectMapper mapper = new ObjectMapper();
		Scanner scanner =
				new Scanner(TransferImporter.class.getResourceAsStream("transferImporter.json"),
						StandardCharsets.UTF_8);
		String json = scanner.useDelimiter("\\A").next();
		TypeReference<List<HashMap<String, String>>> typeRef = new TypeReference<>() { };

		List<HashMap<String, String>> mapList = mapper.readValue(json, typeRef);
		for (HashMap<String, String> map : mapList) {
			// create new transfer
			AccountDTO sender = accountFacade.findByIban(map.get("senderIban"));
			AccountDTO receiver = accountFacade.findByIban(map.get("receiverIban"));

			TransferDTO transfer = new TransferDTO();
			transfer.setSender(sender);
			transfer.setReceiver(receiver);
			transfer.setReceiverName(map.get("receiverName"));
			transfer.setUsage(map.get("usage"));
			transfer.setAmount(Double.parseDouble(map.get("amount")));

			String dateStr = map.get("date");
			if (dateStr == null) { // default today
				transfer.setCreatedOn(new Date());
			} else {
				switch (dateStr) {
				case "LAST_MONTH":
					transfer.setCreatedOn(EssentialUtils.getFirstDate(Period.LAST_MONTH));
					break;

				case "LAST_QUARTER":
					transfer.setCreatedOn(EssentialUtils.getFirstDate(Period.LAST_QUARTER));
					break;

				case "HALF_YEAR":
					transfer.setCreatedOn(EssentialUtils.getFirstDate(Period.HALF_YEAR));
					break;

				default:
					throw new NotSupportedException("Invalid period type: " + dateStr);
				}
			}

			transfer = transferFacade.persist(transfer);
			transferFacade.execute(transfer);
		}

		LOG.info("Import done");
		System.exit(0);
	}
}
