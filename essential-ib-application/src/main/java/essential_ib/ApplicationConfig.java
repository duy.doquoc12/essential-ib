package essential_ib;

import essential_ib.mappers.AccountMapper;
import essential_ib.mappers.AccountToDtoMapper;
import essential_ib.mappers.TransferMapper;
import essential_ib.mappers.TransferToDtoMapper;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class ApplicationConfig {

	@Bean
	public ModelMapper modelMapper() {
		ModelMapper modelMapper = new ModelMapper();
		modelMapper.addMappings(new AccountMapper());
		modelMapper.addMappings(new AccountToDtoMapper());
		modelMapper.addMappings(new TransferMapper());
		modelMapper.addMappings(new TransferToDtoMapper());
		return modelMapper;
	}

	@Bean
	public OpenAPI customOpenAPI() {
		return new OpenAPI()
				.info(new Info()
						.title("Essential-IB API")
						.version("1.0.8"));
	}

	@Bean
	public WebMvcConfigurer corsConfigurer() {
		return new WebMvcConfigurer() {
			@Override
			public void addCorsMappings(CorsRegistry registry) {
				registry.addMapping("/**")
						.allowedOrigins("http://localhost:4200", "https://essential-ib.herokuapp.com");
			}
		};
	}
}
