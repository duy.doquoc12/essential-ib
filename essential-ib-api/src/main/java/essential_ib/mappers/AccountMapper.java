package essential_ib.mappers;

import essential_ib.dtos.AccountDTO;
import essential_ib.entities.Account;
import org.modelmapper.PropertyMap;

/**
 * Map account dto to account
 */
public class AccountMapper extends PropertyMap<AccountDTO, Account> {

	@Override
	protected void configure() {
		map().setTypeByValue(source.getType());
	}
}
