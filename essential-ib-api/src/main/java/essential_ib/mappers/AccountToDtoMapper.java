package essential_ib.mappers;

import essential_ib.dtos.AccountDTO;
import essential_ib.entities.Account;
import org.modelmapper.PropertyMap;

/**
 * Map account to account dto
 */
public class AccountToDtoMapper extends PropertyMap<Account, AccountDTO> {

	@Override
	protected void configure() {
		map().setType(source.getTypeValue());
	}
}
