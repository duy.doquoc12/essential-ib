package essential_ib.mappers;

import essential_ib.dtos.TransferDTO;
import essential_ib.entities.Transfer;
import org.modelmapper.PropertyMap;

public class TransferToDtoMapper extends PropertyMap<Transfer, TransferDTO> {

	@Override
	protected void configure() {
		map().setReceiverName(source.getReceiverName());
	}
}
