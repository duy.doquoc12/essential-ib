package essential_ib.mappers;

import essential_ib.dtos.TransferDTO;
import essential_ib.entities.Transfer;
import org.modelmapper.PropertyMap;

public class TransferMapper extends PropertyMap<TransferDTO, Transfer> {

	@Override
	protected void configure() {
		map().setReceiverName(source.getReceiverName());
	}
}
