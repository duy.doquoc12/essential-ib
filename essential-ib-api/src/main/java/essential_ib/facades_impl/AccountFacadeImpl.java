package essential_ib.facades_impl;

import essential_ib.dtos.AccountDTO;
import essential_ib.entities.Account;
import essential_ib.enums.AccountType;
import essential_ib.facades.AccountFacade;
import essential_ib.services.AccountService;
import essential_ib.services_impl.AccountServiceImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class AccountFacadeImpl implements AccountFacade {
	private static final Logger LOG = LogManager.getLogger(AccountServiceImpl.class);

	private final AccountService accountService;
	private final ModelMapper mapper;

	public AccountFacadeImpl(AccountService accountService, ModelMapper mapper) {
		this.accountService = accountService;
		this.mapper = mapper;
	}

	@Override
	public void persist(AccountDTO accountDTO) {
		accountService.persist(mapper.map(accountDTO, Account.class));
	}

	@Override
	public List<AccountDTO> findByUserId(Long userId) {
		List<AccountDTO> accountDTOs = new ArrayList<>();
		for (Account account : accountService.findByUserId(userId)) {
			accountDTOs.add(mapper.map(account, AccountDTO.class));
		}
		return accountDTOs;
	}

	@Override
	public AccountDTO findByUserIdAndTypeAndName(Long userId, String type, String name) {
		Account account = null;
		try {
			account = accountService.findByUserIdAndTypeAndName(userId, AccountType.findByString(type), name);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return (account != null) ? mapper.map(account, AccountDTO.class) : null;
	}

	@Override
	public AccountDTO findByIban(String iban) {
		Account account = accountService.findByIban(iban);
		if (account != null) {
			return mapper.map(account, AccountDTO.class);
		}

		return null;
	}
}
