package essential_ib.facades_impl;

import essential_ib.dtos.TransferDTO;
import essential_ib.entities.Transfer;
import essential_ib.enums.Period;
import essential_ib.facades.TransferFacade;
import essential_ib.services.TransferService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class TransferFacadeImpl implements TransferFacade {
	private static final Logger LOG = LogManager.getLogger(TransferFacadeImpl.class);

	private final ModelMapper mapper;
	private final TransferService transferService;

	public TransferFacadeImpl(ModelMapper mapper, TransferService transferService) {
		this.mapper = mapper;
		this.transferService = transferService;
	}

	@Override
	public void execute(TransferDTO transfer) {
		transferService.execute(mapper.map(transfer, Transfer.class));
	}

	@Override
	public List<TransferDTO> findTransferListOfAccountForPeriod(Long accId, String period) {
		List<TransferDTO> transferDTOs = new ArrayList<>();
		for (Transfer transfer : transferService.findTransferListOfAccountForPeriod(accId, Period.valueOf(period))) {
			transferDTOs.add(mapper.map(transfer, TransferDTO.class));
		}
		return transferDTOs;
	}

	@Override
	public TransferDTO persist(TransferDTO transfer) {
		return mapper.map(transferService.persist(mapper.map(transfer, Transfer.class)), TransferDTO.class);
	}

	@Override
	public List<TransferDTO> getSavedTransfersOfUser(Long userId) {
		List<TransferDTO> transferDTOs = new ArrayList<>();
		for (Transfer transfer : transferService.getSavedTransfersOfUser(userId)) {
			transferDTOs.add(mapper.map(transfer, TransferDTO.class));
		}
		return transferDTOs;
	}

	@Override
	public TransferDTO findById(Long id) {
		Transfer transfer = transferService.findById(id);
		if (transfer != null) {
			return mapper.map(transfer, TransferDTO.class);
		} else {
			return null;
		}
	}

	@Override
	public void deleteTransfer(TransferDTO transfer) {
		transferService.deleteTransfer(mapper.map(transfer, Transfer.class));
	}
}
