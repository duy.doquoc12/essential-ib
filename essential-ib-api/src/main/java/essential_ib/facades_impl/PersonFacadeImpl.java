package essential_ib.facades_impl;

import essential_ib.dtos.AccountDTO;
import essential_ib.dtos.AddressDTO;
import essential_ib.dtos.PersonDTO;
import essential_ib.entities.Account;
import essential_ib.entities.Address;
import essential_ib.entities.Person;
import essential_ib.facades.PersonFacade;
import essential_ib.services.AccountService;
import essential_ib.services.AddressService;
import essential_ib.services.PersonService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class PersonFacadeImpl implements PersonFacade {
	private static final Logger LOG = LogManager.getLogger(PersonFacadeImpl.class);

	private final PersonService personService;
	private final AccountService accountService;
	private final AddressService addressService;
	private final ModelMapper mapper;

	public PersonFacadeImpl(PersonService personService, AccountService accountService, AddressService addressService,
			ModelMapper mapper) {
		this.personService = personService;
		this.accountService = accountService;
		this.addressService = addressService;
		this.mapper = mapper;
	}

	@Override
	public void persist(PersonDTO personDTO) {
		List<Account> accounts = new ArrayList<>();
		for (AccountDTO account : personDTO.getAccounts()) {
			accounts.add(accountService.findByIban(account.getIban()));
		}

		List<Address> addressList = new ArrayList<>();
		for (AddressDTO address : personDTO.getAddressList()) {
			String street = address.getStreet();
			String houseNumber = address.getHouseNumber();
			String zipCode = address.getZipCode();
			String place = address.getPlace();
			String country = address.getCountry();
			String stair = address.getStair();
			String doorNumber = address.getDoorNumber();

			addressList
					.add(addressService.find(street, houseNumber, zipCode, place, country, stair, doorNumber).get(0));
		}

		Person person = mapper.map(personDTO, Person.class);
		person.setAccounts(accounts);
		person.setAddressList(addressList);

		personService.persist(person);
	}
}
