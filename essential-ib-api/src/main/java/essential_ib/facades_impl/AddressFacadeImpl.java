package essential_ib.facades_impl;

import essential_ib.dtos.AddressDTO;
import essential_ib.entities.Address;
import essential_ib.facades.AddressFacade;
import essential_ib.services.AddressService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class AddressFacadeImpl implements AddressFacade {
	private static final Logger LOG = LogManager.getLogger(AddressFacadeImpl.class);

	private final AddressService addressService;
	private final ModelMapper mapper;

	public AddressFacadeImpl(AddressService addressService, ModelMapper mapper) {
		this.addressService = addressService;
		this.mapper = mapper;
	}

	@Override
	public void persist(AddressDTO addressDTO) {
		addressService.persist(mapper.map(addressDTO, Address.class));
	}

	@Override
	public List<AddressDTO> find(String street, String houseNumber, String zipCode, String place, String country,
			String stair, String doorNumber) {
		List<AddressDTO> addressDTOs = new ArrayList<>();
		for (Address address : addressService.find(street, houseNumber, zipCode, place, country, stair, doorNumber)) {
			addressDTOs.add(mapper.map(address, AddressDTO.class));
		}
		return addressDTOs;
	}
}
