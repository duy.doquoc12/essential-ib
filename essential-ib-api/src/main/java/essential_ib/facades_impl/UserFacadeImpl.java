package essential_ib.facades_impl;

import essential_ib.dtos.OrganizationDTO;
import essential_ib.dtos.PersonDTO;
import essential_ib.dtos.UserDTO;
import essential_ib.entities.Person;
import essential_ib.entities.User;
import essential_ib.facades.UserFacade;
import essential_ib.services.UserService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
public class UserFacadeImpl implements UserFacade {
	private static final Logger LOG = LogManager.getLogger(UserFacadeImpl.class);

	private final UserService userService;
	private final ModelMapper mapper;

	public UserFacadeImpl(UserService userService, ModelMapper mapper) {
		this.userService = userService;
		this.mapper = mapper;
	}

	@Override
	public UserDTO findByUserNameAndPassword(String userName, String password) {
		User user = userService.findByUserNameAndPassword(userName, password);
		if (user == null) {
			return null;
		} else if (user instanceof Person){
			return mapper.map(user, PersonDTO.class);
		} else {
			return mapper.map(user, OrganizationDTO.class);
		}
	}

	@Override
	public UserDTO findById(Long id) {
		User user = userService.findById(id);
		if (user == null) {
			return null;
		} else if (user instanceof Person){
			return mapper.map(user, PersonDTO.class);
		} else {
			return mapper.map(user, OrganizationDTO.class);
		}
	}
}
