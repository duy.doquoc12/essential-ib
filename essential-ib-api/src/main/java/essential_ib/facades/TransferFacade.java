package essential_ib.facades;

import essential_ib.dtos.TransferDTO;

import java.util.List;

public interface TransferFacade {

	/**
	 * Transfer given amount of money from sender account to receiver account.
	 *
	 * @param transfer transfer object containing all information of the transfer
	 */
	void execute(TransferDTO transfer);

	/**
	 * Find all transfer of the given account within the given period.
	 *
	 * @param accId  id of account
	 * @param period period
	 * @return list of transfers
	 */
	List<TransferDTO> findTransferListOfAccountForPeriod(Long accId, String period);

	/**
	 * Create new transfer or update already existing ones with given transfer.
	 *
	 * @param transfer transfer
	 * @return saved or update transfer
	 */
	TransferDTO persist(TransferDTO transfer);

	/**
	 * Get all transfers of an user, where the saved attribute is true.<br>
	 * Order by create date descending.
	 */
	List<TransferDTO> getSavedTransfersOfUser(Long userId);

	/**
	 * Find transfer with given id.
	 *
	 * @param id transfer id
	 * @return transfer with given id; or null if not exist
	 */
	TransferDTO findById(Long id);

	/**
	 * Delete given transfer.
	 *
	 * @param transfer transfer to be deleted
	 */
	void deleteTransfer(TransferDTO transfer);
}
