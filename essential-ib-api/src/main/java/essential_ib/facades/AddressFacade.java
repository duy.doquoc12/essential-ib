package essential_ib.facades;

import essential_ib.dtos.AddressDTO;

import java.util.List;

public interface AddressFacade {

	/**
	 * If address doesn't exist, then create a new address.<br>
	 * Else update old address.
	 *
	 * @param addressDTO address
	 */
	void persist(AddressDTO addressDTO);

	/**
	 * Find address by all its attributes.
	 *
	 * @param street      street
	 * @param houseNumber houseNumber
	 * @param zipCode     zipCode
	 * @param place       place
	 * @param country     country
	 * @param stair       stair
	 * @param doorNumber  doorNumber
	 * @return address list
	 */
	List<AddressDTO> find(String street, String houseNumber, String zipCode, String place, String country,
			String stair, String doorNumber);
}
