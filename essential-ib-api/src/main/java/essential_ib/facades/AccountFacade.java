package essential_ib.facades;

import essential_ib.dtos.AccountDTO;

import java.util.List;

public interface AccountFacade {

	/**
	 * If account doesn't exist, then create a new account.<br>
	 * Else update old account.
	 *
	 * @param accountDTO account
	 */
	void persist(AccountDTO accountDTO);

	/**
	 * Find all accounts of an user.
	 *
	 * @param userId user ID
	 * @return list of accounts
	 */
	List<AccountDTO> findByUserId(Long userId);

	/**
	 * Find account of user with user id and account type and name.
	 *
	 * @param userId user id
	 * @param type   account type value/name
	 * @param name   account name
	 * @return account; or null if not exists
	 */
	AccountDTO findByUserIdAndTypeAndName(Long userId, String type, String name);

	/**
	 * Find account by its account id.
	 *
	 * @param iban account id
	 * @return account; or null if not exists
	 */
	AccountDTO findByIban(String iban);

}
