package essential_ib.facades;

import essential_ib.dtos.PersonDTO;

public interface PersonFacade {

	/**
	 * If person doesn't exist, then create a new person.<br>
	 * Else update old person.
	 *
	 * @param personDTO person
	 */
	void persist(PersonDTO personDTO);

}
