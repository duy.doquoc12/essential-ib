package essential_ib.facades;

import essential_ib.dtos.UserDTO;

public interface UserFacade {

	/**
	 * Find user by its user name and password.
	 *
	 * @param userName user name
	 * @param password password
	 * @return user with user name and password; or null if not exists
	 */
	UserDTO findByUserNameAndPassword(String userName, String password);

	/**
	 * Find user by its id.
	 *
	 * @param id ID
	 * @return user with id; or null if not exists
	 */
	UserDTO findById(Long id);
}
