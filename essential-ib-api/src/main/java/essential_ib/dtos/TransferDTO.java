package essential_ib.dtos;

import java.util.Date;

public class TransferDTO {

	private Long id;
	private Date createdOn;
	private String createdOnStr;
	private AccountDTO sender;
	private String receiverName;
	private AccountDTO receiver;
	private String usage;
	private double amount;
	private boolean saved;

	public AccountDTO getSender() {
		return sender;
	}

	public void setSender(AccountDTO sender) {
		this.sender = sender;
	}

	public String getReceiverName() {
		return receiverName;
	}

	public void setReceiverName(String receiverName) {
		this.receiverName = receiverName;
	}

	public AccountDTO getReceiver() {
		return receiver;
	}

	public void setReceiver(AccountDTO receiver) {
		this.receiver = receiver;
	}

	public String getUsage() {
		return usage;
	}

	public void setUsage(String usage) {
		this.usage = usage;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public boolean isSaved() {
		return saved;
	}

	public void setSaved(boolean saved) {
		this.saved = saved;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCreatedOnStr() {
		return createdOnStr;
	}

	public void setCreatedOnStr(String createdOnStr) {
		this.createdOnStr = createdOnStr;
	}
}
